/**
 * @file hackaBLE.ino
 * @brief hackable arduino sample code * @version 0.2
 * @date 2019-01-25
 * 
 * @copyright Copyright (c) 2019, Electronut Labs (electronut.in)
 * 
 */

// Import libraries (BLEPeripheral depends on SPI)
#include <SPI.h>
#include <BLEPeripheral.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme;

unsigned long delayTime;

int g_old_val = 0;    // last temperature reading from analog input
long g_previousMillis = 0; // last time the temperature was checked, in ms

// LED pin
#define LED_PIN LED_BUILTIN

//custom boards may override default pin definitions with BLEPeripheral(PIN_REQ, PIN_RDY, PIN_RST)
BLEPeripheral blePeripheral = BLEPeripheral();

// create service
BLEService update_tempService = BLEService("19b10000e8f2537e4f6cd104768a1214");

// create switch characteristic
BLECharCharacteristic updateCharacteristic = BLECharCharacteristic("19b10001e8f2537e4f6cd104768a1214", BLERead | BLENotify);

void setup()
{
  Serial.begin(115200);
#if defined(__AVR_ATmega32U4__)
  delay(5000); //5 seconds delay for enabling to see the start up comments on the serial board
#endif

  // set LED pin to output mode
  pinMode(LED_PIN, OUTPUT);

  // set advertised local name and service UUID
  blePeripheral.setLocalName("BME_Collect");
  blePeripheral.setAdvertisedServiceUuid(update_tempService.uuid());

  // add service and characteristic
  blePeripheral.addAttribute(update_tempService);
  blePeripheral.addAttribute(updateCharacteristic);

  // assign event handlers for connected, disconnected to peripheral
  blePeripheral.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  blePeripheral.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);

  // assign event handlers for characteristic
  updateCharacteristic.setEventHandler(BLESubscribed, characteristicSubscribed);
  updateCharacteristic.setEventHandler(BLEUnsubscribed, characteristicUnsubscribed);

  // begin initialization
  blePeripheral.begin();

  bool status;

  // default settings
  // (you can also pass in a Wire library object like &Wire2)
  status = bme.begin(&Wire);
  if (!status)
  {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1)
      ;
  }

  delayTime = 1000;

  Serial.println(F("BLE advertise:update temperature data"));
}

void loop()
{

    BLECentral central = blePeripheral.central();

  if (central)
  {
    Serial.print("Connected to central: ");
    // print the central's MAC address:
    Serial.println(central.address());
    // turn on the LED to indicate the connection:
    digitalWrite(13, HIGH);

    // check the temprature measurement every 1000ms
    // as long as the central is still connected:
    while (central.connected())
    {
      long currentMillis = millis();
      // if 200ms have passed, check the temperature measurement:
      if (currentMillis - previousMillis >= 1000)
      {
        previousMillis = currentMillis;
       update_temperature_data(central, updateCharacteristic);
      }
    }
    // when the central disconnects, turn off the LED:
   // digitalWrite(13, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central.address());
  }
}

void blePeripheralConnectHandler(BLECentral &central)
{
  // central connected event handler
  Serial.print(F("Connected event, central: "));
  Serial.println(central.address());
}

void blePeripheralDisconnectHandler(BLECentral &central)
{
  // central disconnected event handler
  Serial.print(F("Disconnected event, central: "));
  Serial.println(central.address());
}

void characteristicSubscribed(BLECentral &central, BLECharacteristic &characteristic)
{
  // characteristic subscribed event handler
  Serial.println(F("Characteristic event, subscribed"));
}

void characteristicUnsubscribed(BLECentral &central, BLECharacteristic &characteristic)
{
  // characteristic unsubscribed event handler
  Serial.println(F("Characteristic event, unsubscribed"));
}

void update_temperature_data(BLECentral &central, BLECharacteristic &characteristic)
{
  // central wrote new value to characteristic, update LED
  Serial.println("updated temperature value:");
  Serial.print("Temperature = ");
  Serial.print(bme.readTemperature());
  Serial.println(" *C");

  updateCharacteristic.setValue(bme.readTemperature());
}
